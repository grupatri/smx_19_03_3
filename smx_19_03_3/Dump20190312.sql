-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dnevnik_gr_3
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (8);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `korisnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(45) NOT NULL,
  `Prezime` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `Odeljenje_id_odeljenje` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_Zaposleni_Odeljenje1_idx` (`Odeljenje_id_odeljenje`),
  CONSTRAINT `fk_Zaposleni_Odeljenje1` FOREIGN KEY (`Odeljenje_id_odeljenje`) REFERENCES `odeljenje` (`id_odeljenje`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (4,'Pera','Peric','peki','1234',NULL),(5,'Pera2','Peric2','peki2','12345',NULL),(6,'Pera3','Peric3','peki3','1234',NULL);
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocena`
--

DROP TABLE IF EXISTS `ocena`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocena` (
  `id` int(11) NOT NULL,
  `id_predmeta` int(11) DEFAULT NULL,
  `id_ucenika` int(11) DEFAULT NULL,
  `ocena` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocena`
--

LOCK TABLES `ocena` WRITE;
/*!40000 ALTER TABLE `ocena` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocena` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocene`
--

DROP TABLE IF EXISTS `ocene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ucenika` int(11) DEFAULT NULL,
  `id_predmeta` int(11) DEFAULT NULL,
  `ocena` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocene`
--

LOCK TABLES `ocene` WRITE;
/*!40000 ALTER TABLE `ocene` DISABLE KEYS */;
INSERT INTO `ocene` VALUES (1,1,1,4);
/*!40000 ALTER TABLE `ocene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocene_has_ucenik`
--

DROP TABLE IF EXISTS `ocene_has_ucenik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocene_has_ucenik` (
  `Ucenik_id_ucenik` int(11) NOT NULL,
  `Ocene_id` int(11) NOT NULL,
  PRIMARY KEY (`Ucenik_id_ucenik`,`Ocene_id`),
  KEY `fk_Ocene_has_Ucenik_Ucenik1_idx` (`Ucenik_id_ucenik`),
  KEY `fk_Ocene_has_Ucenik_Ocene1_idx` (`Ocene_id`),
  CONSTRAINT `fk_Ocene_has_Ucenik_Ocene1` FOREIGN KEY (`Ocene_id`) REFERENCES `ocene` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ocene_has_Ucenik_Ucenik1` FOREIGN KEY (`Ucenik_id_ucenik`) REFERENCES `ucenik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocene_has_ucenik`
--

LOCK TABLES `ocene_has_ucenik` WRITE;
/*!40000 ALTER TABLE `ocene_has_ucenik` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocene_has_ucenik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `odeljenje`
--

DROP TABLE IF EXISTS `odeljenje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `odeljenje` (
  `id_odeljenje` smallint(2) NOT NULL AUTO_INCREMENT,
  `id_ucitelja` int(11) DEFAULT NULL,
  `id_ucenika` int(11) DEFAULT NULL,
  `BrojUcenika` varchar(45) DEFAULT NULL,
  `ImeUcitelja` varchar(45) DEFAULT NULL,
  `Raspored_id_raspored` int(11) NOT NULL,
  PRIMARY KEY (`id_odeljenje`),
  KEY `fk_Odeljenje_Raspored1_idx` (`Raspored_id_raspored`),
  CONSTRAINT `fk_Odeljenje_Raspored1` FOREIGN KEY (`Raspored_id_raspored`) REFERENCES `raspored` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odeljenje`
--

LOCK TABLES `odeljenje` WRITE;
/*!40000 ALTER TABLE `odeljenje` DISABLE KEYS */;
/*!40000 ALTER TABLE `odeljenje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predmet`
--

DROP TABLE IF EXISTS `predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predmet` (
  `id_predmet` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `id_ucenika` int(11) NOT NULL,
  `ocena` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id_predmet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predmet`
--

LOCK TABLES `predmet` WRITE;
/*!40000 ALTER TABLE `predmet` DISABLE KEYS */;
/*!40000 ALTER TABLE `predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predmet_has_ocene`
--

DROP TABLE IF EXISTS `predmet_has_ocene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predmet_has_ocene` (
  `Predmet_id_predmet` int(11) NOT NULL,
  `Ocene_id` int(11) NOT NULL,
  PRIMARY KEY (`Predmet_id_predmet`,`Ocene_id`),
  KEY `fk_Predmet_has_Ocene_Ocene1_idx` (`Ocene_id`),
  KEY `fk_Predmet_has_Ocene_Predmet1_idx` (`Predmet_id_predmet`),
  CONSTRAINT `fk_Predmet_has_Ocene_Ocene1` FOREIGN KEY (`Ocene_id`) REFERENCES `ocene` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Predmet_has_Ocene_Predmet1` FOREIGN KEY (`Predmet_id_predmet`) REFERENCES `predmet` (`id_predmet`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predmet_has_ocene`
--

LOCK TABLES `predmet_has_ocene` WRITE;
/*!40000 ALTER TABLE `predmet_has_ocene` DISABLE KEYS */;
/*!40000 ALTER TABLE `predmet_has_ocene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raspored`
--

DROP TABLE IF EXISTS `raspored`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raspored` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vreme` datetime NOT NULL,
  `id_predmeta` int(11) NOT NULL,
  `id_odeljenja` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raspored`
--

LOCK TABLES `raspored` WRITE;
/*!40000 ALTER TABLE `raspored` DISABLE KEYS */;
/*!40000 ALTER TABLE `raspored` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raspored_has_predmet`
--

DROP TABLE IF EXISTS `raspored_has_predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raspored_has_predmet` (
  `Raspored_id_raspored` int(11) NOT NULL,
  `Predmet_id_predmet` int(11) NOT NULL,
  PRIMARY KEY (`Raspored_id_raspored`,`Predmet_id_predmet`),
  KEY `fk_Raspored_has_Predmet_Predmet1_idx` (`Predmet_id_predmet`),
  KEY `fk_Raspored_has_Predmet_Raspored1_idx` (`Raspored_id_raspored`),
  CONSTRAINT `fk_Raspored_has_Predmet_Predmet1` FOREIGN KEY (`Predmet_id_predmet`) REFERENCES `predmet` (`id_predmet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Raspored_has_Predmet_Raspored1` FOREIGN KEY (`Raspored_id_raspored`) REFERENCES `raspored` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raspored_has_predmet`
--

LOCK TABLES `raspored_has_predmet` WRITE;
/*!40000 ALTER TABLE `raspored_has_predmet` DISABLE KEYS */;
/*!40000 ALTER TABLE `raspored_has_predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roditelj`
--

DROP TABLE IF EXISTS `roditelj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roditelj` (
  `id_roditelj` int(11) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(45) NOT NULL,
  `Prezime` varchar(45) NOT NULL,
  `Id_ucenika` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id_roditelj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roditelj`
--

LOCK TABLES `roditelj` WRITE;
/*!40000 ALTER TABLE `roditelj` DISABLE KEYS */;
/*!40000 ALTER TABLE `roditelj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roditelj_has_ucenik`
--

DROP TABLE IF EXISTS `roditelj_has_ucenik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roditelj_has_ucenik` (
  `Roditelj_id_roditelj` int(11) NOT NULL,
  `Ucenik_id_ucenik` int(11) NOT NULL,
  PRIMARY KEY (`Roditelj_id_roditelj`,`Ucenik_id_ucenik`),
  KEY `fk_Roditelj_has_Ucenik_Ucenik1_idx` (`Ucenik_id_ucenik`),
  KEY `fk_Roditelj_has_Ucenik_Roditelj1_idx` (`Roditelj_id_roditelj`),
  CONSTRAINT `fk_Roditelj_has_Ucenik_Roditelj1` FOREIGN KEY (`Roditelj_id_roditelj`) REFERENCES `roditelj` (`id_roditelj`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Roditelj_has_Ucenik_Ucenik1` FOREIGN KEY (`Ucenik_id_ucenik`) REFERENCES `ucenik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roditelj_has_ucenik`
--

LOCK TABLES `roditelj_has_ucenik` WRITE;
/*!40000 ALTER TABLE `roditelj_has_ucenik` DISABLE KEYS */;
/*!40000 ALTER TABLE `roditelj_has_ucenik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ucenik`
--

DROP TABLE IF EXISTS `ucenik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ucenik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(45) NOT NULL,
  `Prezime` varchar(45) NOT NULL,
  `jmbg` varchar(13) NOT NULL,
  `Id_ucitelja` int(11) DEFAULT NULL,
  `Id_odeljenja` int(11) DEFAULT NULL,
  `izostanci` varchar(45) DEFAULT NULL,
  `Odeljenje_id_odeljenje` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Ucenik_Odeljenje1_idx` (`Odeljenje_id_odeljenje`),
  CONSTRAINT `fk_Ucenik_Odeljenje1` FOREIGN KEY (`Odeljenje_id_odeljenje`) REFERENCES `odeljenje` (`id_odeljenje`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ucenik`
--

LOCK TABLES `ucenik` WRITE;
/*!40000 ALTER TABLE `ucenik` DISABLE KEYS */;
INSERT INTO `ucenik` VALUES (1,'Jovan','Jovanovic','123456789',NULL,NULL,'0',NULL),(2,'Marko','Markovic','123456788',NULL,NULL,'4',NULL),(4,'Pera','Peric','123456789',NULL,NULL,'20',NULL);
/*!40000 ALTER TABLE `ucenik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ucenik_has_predmet`
--

DROP TABLE IF EXISTS `ucenik_has_predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ucenik_has_predmet` (
  `Ucenik_id_ucenik` int(11) NOT NULL,
  `Predmet_id_predmet` int(11) NOT NULL,
  PRIMARY KEY (`Ucenik_id_ucenik`,`Predmet_id_predmet`),
  KEY `fk_Ucenik_has_Predmet_Predmet1_idx` (`Predmet_id_predmet`),
  KEY `fk_Ucenik_has_Predmet_Ucenik1_idx` (`Ucenik_id_ucenik`),
  CONSTRAINT `fk_Ucenik_has_Predmet_Predmet1` FOREIGN KEY (`Predmet_id_predmet`) REFERENCES `predmet` (`id_predmet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ucenik_has_Predmet_Ucenik1` FOREIGN KEY (`Ucenik_id_ucenik`) REFERENCES `ucenik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ucenik_has_predmet`
--

LOCK TABLES `ucenik_has_predmet` WRITE;
/*!40000 ALTER TABLE `ucenik_has_predmet` DISABLE KEYS */;
/*!40000 ALTER TABLE `ucenik_has_predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uloge`
--

DROP TABLE IF EXISTS `uloge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uloge` (
  `iduloge` int(11) NOT NULL AUTO_INCREMENT,
  `administrator` varchar(45) DEFAULT NULL,
  `ucitelj` varchar(45) DEFAULT NULL,
  `roditelj` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iduloge`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uloge`
--

LOCK TABLES `uloge` WRITE;
/*!40000 ALTER TABLE `uloge` DISABLE KEYS */;
/*!40000 ALTER TABLE `uloge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zakazivanje`
--

DROP TABLE IF EXISTS `zakazivanje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zakazivanje` (
  `id_zakazivanje` int(11) NOT NULL AUTO_INCREMENT,
  `id_roditelja` int(11) NOT NULL,
  `id_ucitelja` int(11) NOT NULL,
  `termin` datetime NOT NULL,
  `odobreno` varchar(5) DEFAULT NULL,
  `Roditelj_id_roditelj` int(11) NOT NULL,
  `Zaposleni_id_zaposleni` int(11) NOT NULL,
  PRIMARY KEY (`id_zakazivanje`),
  KEY `fk_Zakazivanje_Roditelj_idx` (`Roditelj_id_roditelj`),
  KEY `fk_Zakazivanje_Zaposleni1_idx` (`Zaposleni_id_zaposleni`),
  CONSTRAINT `fk_Zakazivanje_Roditelj` FOREIGN KEY (`Roditelj_id_roditelj`) REFERENCES `roditelj` (`id_roditelj`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Zakazivanje_Zaposleni1` FOREIGN KEY (`Zaposleni_id_zaposleni`) REFERENCES `korisnik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zakazivanje`
--

LOCK TABLES `zakazivanje` WRITE;
/*!40000 ALTER TABLE `zakazivanje` DISABLE KEYS */;
/*!40000 ALTER TABLE `zakazivanje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-12 13:29:57
