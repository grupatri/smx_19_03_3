CREATE DATABASE  IF NOT EXISTS `dnevnik_gr_3` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dnevnik_gr_3`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: dnevnik_gr_3
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (8),(8),(8),(8),(8),(8),(8);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `korisnik` (
  `id` int(11) NOT NULL,
  `Ime` varchar(45) DEFAULT NULL,
  `Prezime` varchar(45) DEFAULT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(90) NOT NULL,
  `Odeljenje_id_odeljenje` smallint(2) DEFAULT NULL,
  `ucenik_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `UK_d7tnrllt9olfunnurk0gb24k9` (`username`),
  KEY `fk_Zaposleni_Odeljenje1_idx` (`Odeljenje_id_odeljenje`),
  KEY `FKq0i0l06hxv6wrwoa4w651uoe2` (`ucenik_id`),
  CONSTRAINT `FKq0i0l06hxv6wrwoa4w651uoe2` FOREIGN KEY (`ucenik_id`) REFERENCES `ucenik` (`id`),
  CONSTRAINT `fk_Zaposleni_Odeljenje1` FOREIGN KEY (`Odeljenje_id_odeljenje`) REFERENCES `odeljenje` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (4,'Pera','Peric','peki','1234',NULL,NULL),(5,'Pera2','Peric2','peki2','12345',NULL,NULL),(6,'Pera3','Peric3','peki3','1234',NULL,NULL),(7,NULL,NULL,'blabla','$2a$10$jx9b3wKIr2A9dauRX5Ggd.Ejl2KH0vjay7k8Q8ZYutHKv6pq7bw.K',NULL,NULL),(9,NULL,NULL,'admin123','$2a$10$bZFab2ZDKbfP70pXOuWhKO/hgZl.EpqgdqQ3jGawn/bt94jaBJDD.',NULL,NULL),(10,NULL,NULL,'admin1234','admin12345',NULL,NULL),(11,'Donald','Duck','donald','12345',NULL,NULL),(13,'Duffy','Duck','ducky','$2a$10$jQmyhB/PFZ6j/.rYl4p7YekgrT0PXLEgJi5q7taPJvWxtxI3pcq6O',NULL,NULL);
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik_ucenik`
--

DROP TABLE IF EXISTS `korisnik_ucenik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `korisnik_ucenik` (
  `korisnik_id` int(11) NOT NULL,
  `ucenik_id` int(11) NOT NULL,
  UNIQUE KEY `UK_7kgaagnnqk2vmbsdtiggfaiwy` (`ucenik_id`),
  KEY `FKdofnyt6np0m3k4t6o5a18fvmq` (`korisnik_id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik_ucenik`
--

LOCK TABLES `korisnik_ucenik` WRITE;
/*!40000 ALTER TABLE `korisnik_ucenik` DISABLE KEYS */;
INSERT INTO `korisnik_ucenik` VALUES (13,1);
/*!40000 ALTER TABLE `korisnik_ucenik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik_uloga`
--

DROP TABLE IF EXISTS `korisnik_uloga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `korisnik_uloga` (
  `korisnik_id` int(11) NOT NULL,
  `uloga_id` int(11) NOT NULL,
  KEY `FKchqo562qjtowrxby7dxec57e7` (`uloga_id`),
  KEY `FKq2bypw644gsehctpydyingxqh` (`korisnik_id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik_uloga`
--

LOCK TABLES `korisnik_uloga` WRITE;
/*!40000 ALTER TABLE `korisnik_uloga` DISABLE KEYS */;
INSERT INTO `korisnik_uloga` VALUES (9,1),(7,2),(13,4);
/*!40000 ALTER TABLE `korisnik_uloga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocena`
--

DROP TABLE IF EXISTS `ocena`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocena` (
  `id` int(11) NOT NULL,
  `id_predmeta` int(11) DEFAULT NULL,
  `id_ucenika` int(11) DEFAULT NULL,
  `ocena` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocena`
--

LOCK TABLES `ocena` WRITE;
/*!40000 ALTER TABLE `ocena` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocena` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocene`
--

DROP TABLE IF EXISTS `ocene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocene` (
  `id` int(11) NOT NULL,
  `id_ucenika` int(11) DEFAULT NULL,
  `id_predmeta` int(11) DEFAULT NULL,
  `ocena` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocene`
--

LOCK TABLES `ocene` WRITE;
/*!40000 ALTER TABLE `ocene` DISABLE KEYS */;
INSERT INTO `ocene` VALUES (1,1,1,4);
/*!40000 ALTER TABLE `ocene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocene_has_ucenik`
--

DROP TABLE IF EXISTS `ocene_has_ucenik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocene_has_ucenik` (
  `Ucenik_id_ucenik` int(11) NOT NULL,
  `Ocene_id` int(11) NOT NULL,
  PRIMARY KEY (`Ucenik_id_ucenik`,`Ocene_id`),
  KEY `fk_Ocene_has_Ucenik_Ucenik1_idx` (`Ucenik_id_ucenik`),
  KEY `fk_Ocene_has_Ucenik_Ocene1_idx` (`Ocene_id`),
  CONSTRAINT `fk_Ocene_has_Ucenik_Ocene1` FOREIGN KEY (`Ocene_id`) REFERENCES `ocene` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ocene_has_Ucenik_Ucenik1` FOREIGN KEY (`Ucenik_id_ucenik`) REFERENCES `ucenik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocene_has_ucenik`
--

LOCK TABLES `ocene_has_ucenik` WRITE;
/*!40000 ALTER TABLE `ocene_has_ucenik` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocene_has_ucenik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `odeljenje`
--

DROP TABLE IF EXISTS `odeljenje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `odeljenje` (
  `id` smallint(2) NOT NULL,
  `broj_odeljenja` smallint(2) DEFAULT NULL,
  `id_ucitelja` int(11) DEFAULT NULL,
  `id_ucenika` int(11) DEFAULT NULL,
  `Raspored_id_raspored` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Odeljenje_Raspored1_idx` (`Raspored_id_raspored`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odeljenje`
--

LOCK TABLES `odeljenje` WRITE;
/*!40000 ALTER TABLE `odeljenje` DISABLE KEYS */;
/*!40000 ALTER TABLE `odeljenje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistent_logins`
--

DROP TABLE IF EXISTS `persistent_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `persistent_logins` (
  `username` varchar(100) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL,
  PRIMARY KEY (`series`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistent_logins`
--

LOCK TABLES `persistent_logins` WRITE;
/*!40000 ALTER TABLE `persistent_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `persistent_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poruke`
--

DROP TABLE IF EXISTS `poruke`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `poruke` (
  `id` int(11) NOT NULL,
  `sadrzaj` varchar(255) DEFAULT NULL,
  `content` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poruke`
--

LOCK TABLES `poruke` WRITE;
/*!40000 ALTER TABLE `poruke` DISABLE KEYS */;
INSERT INTO `poruke` VALUES (2,NULL,''),(3,NULL,'dsdsdsds'),(4,NULL,'fgfgfgfg'),(7,NULL,'yyyy');
/*!40000 ALTER TABLE `poruke` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predmet`
--

DROP TABLE IF EXISTS `predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `predmet` (
  `id_predmet` int(11) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  `id_ucenika` int(11) NOT NULL,
  `ocena` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id_predmet`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predmet`
--

LOCK TABLES `predmet` WRITE;
/*!40000 ALTER TABLE `predmet` DISABLE KEYS */;
/*!40000 ALTER TABLE `predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predmet_has_ocene`
--

DROP TABLE IF EXISTS `predmet_has_ocene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `predmet_has_ocene` (
  `Predmet_id_predmet` int(11) NOT NULL,
  `Ocene_id` int(11) NOT NULL,
  PRIMARY KEY (`Predmet_id_predmet`,`Ocene_id`),
  KEY `fk_Predmet_has_Ocene_Ocene1_idx` (`Ocene_id`),
  KEY `fk_Predmet_has_Ocene_Predmet1_idx` (`Predmet_id_predmet`),
  CONSTRAINT `fk_Predmet_has_Ocene_Ocene1` FOREIGN KEY (`Ocene_id`) REFERENCES `ocene` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Predmet_has_Ocene_Predmet1` FOREIGN KEY (`Predmet_id_predmet`) REFERENCES `predmet` (`id_predmet`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predmet_has_ocene`
--

LOCK TABLES `predmet_has_ocene` WRITE;
/*!40000 ALTER TABLE `predmet_has_ocene` DISABLE KEYS */;
/*!40000 ALTER TABLE `predmet_has_ocene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raspored`
--

DROP TABLE IF EXISTS `raspored`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `raspored` (
  `id` int(11) NOT NULL,
  `dan_u_nedelji` varchar(10) DEFAULT NULL,
  `vreme` varchar(5) NOT NULL,
  `id_odeljenja` int(11) NOT NULL,
  `id_predmeta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raspored`
--

LOCK TABLES `raspored` WRITE;
/*!40000 ALTER TABLE `raspored` DISABLE KEYS */;
INSERT INTO `raspored` VALUES (1,'Ponedeljak','09:00',2,3),(8,'ponedeljak','09:50',2,4);
/*!40000 ALTER TABLE `raspored` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raspored_has_predmet`
--

DROP TABLE IF EXISTS `raspored_has_predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `raspored_has_predmet` (
  `Raspored_id_raspored` int(11) NOT NULL,
  `Predmet_id_predmet` int(11) NOT NULL,
  PRIMARY KEY (`Raspored_id_raspored`,`Predmet_id_predmet`),
  KEY `fk_Raspored_has_Predmet_Predmet1_idx` (`Predmet_id_predmet`),
  KEY `fk_Raspored_has_Predmet_Raspored1_idx` (`Raspored_id_raspored`),
  CONSTRAINT `fk_Raspored_has_Predmet_Predmet1` FOREIGN KEY (`Predmet_id_predmet`) REFERENCES `predmet` (`id_predmet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Raspored_has_Predmet_Raspored1` FOREIGN KEY (`Raspored_id_raspored`) REFERENCES `raspored` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raspored_has_predmet`
--

LOCK TABLES `raspored_has_predmet` WRITE;
/*!40000 ALTER TABLE `raspored_has_predmet` DISABLE KEYS */;
/*!40000 ALTER TABLE `raspored_has_predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roditelj`
--

DROP TABLE IF EXISTS `roditelj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roditelj` (
  `id_roditelj` int(11) NOT NULL,
  `Ime` varchar(45) NOT NULL,
  `Prezime` varchar(45) NOT NULL,
  `Id_ucenika` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id_roditelj`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roditelj`
--

LOCK TABLES `roditelj` WRITE;
/*!40000 ALTER TABLE `roditelj` DISABLE KEYS */;
/*!40000 ALTER TABLE `roditelj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ucenik`
--

DROP TABLE IF EXISTS `ucenik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ucenik` (
  `id` int(11) NOT NULL,
  `Ime` varchar(45) NOT NULL,
  `Prezime` varchar(45) NOT NULL,
  `jmbg` varchar(13) NOT NULL,
  `Id_ucitelja` int(11) DEFAULT NULL,
  `Id_odeljenja` int(11) DEFAULT NULL,
  `izostanci` varchar(45) DEFAULT NULL,
  `Odeljenje_id_odeljenje` smallint(2) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `korisnik_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Ucenik_Odeljenje1_idx` (`Odeljenje_id_odeljenje`),
  KEY `ucenik_username_idx` (`username`),
  CONSTRAINT `fk_Ucenik_Odeljenje1` FOREIGN KEY (`Odeljenje_id_odeljenje`) REFERENCES `odeljenje` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ucenik_username` FOREIGN KEY (`username`) REFERENCES `korisnik` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ucenik`
--

LOCK TABLES `ucenik` WRITE;
/*!40000 ALTER TABLE `ucenik` DISABLE KEYS */;
INSERT INTO `ucenik` VALUES (1,'Jovan','Jovanovic','123456789',NULL,NULL,'0',NULL,'ducky',NULL),(2,'Marko','Markovic','123456788',NULL,NULL,'4',NULL,NULL,NULL),(4,'Pera','Peric','123456789',NULL,NULL,'20',NULL,NULL,NULL);
/*!40000 ALTER TABLE `ucenik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ucenik_has_predmet`
--

DROP TABLE IF EXISTS `ucenik_has_predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ucenik_has_predmet` (
  `Ucenik_id_ucenik` int(11) NOT NULL,
  `Predmet_id_predmet` int(11) NOT NULL,
  PRIMARY KEY (`Ucenik_id_ucenik`,`Predmet_id_predmet`),
  KEY `fk_Ucenik_has_Predmet_Predmet1_idx` (`Predmet_id_predmet`),
  KEY `fk_Ucenik_has_Predmet_Ucenik1_idx` (`Ucenik_id_ucenik`),
  CONSTRAINT `fk_Ucenik_has_Predmet_Predmet1` FOREIGN KEY (`Predmet_id_predmet`) REFERENCES `predmet` (`id_predmet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ucenik_has_Predmet_Ucenik1` FOREIGN KEY (`Ucenik_id_ucenik`) REFERENCES `ucenik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ucenik_has_predmet`
--

LOCK TABLES `ucenik_has_predmet` WRITE;
/*!40000 ALTER TABLE `ucenik_has_predmet` DISABLE KEYS */;
/*!40000 ALTER TABLE `ucenik_has_predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uloga`
--

DROP TABLE IF EXISTS `uloga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `uloga` (
  `id` int(11) NOT NULL,
  `ime` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_4pjpw35of2pk0col8c3fk2gx3` (`ime`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uloga`
--

LOCK TABLES `uloga` WRITE;
/*!40000 ALTER TABLE `uloga` DISABLE KEYS */;
INSERT INTO `uloga` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER'),(3,'ROLE_UCITELJ'),(4,'ROLE_RODITELJ'),(5,'ROLE_DIREKTOR');
/*!40000 ALTER TABLE `uloga` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
