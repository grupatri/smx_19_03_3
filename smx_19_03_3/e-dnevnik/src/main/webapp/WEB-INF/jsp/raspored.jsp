<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title>Welcome</title>
<link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
rel="nofollow">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
  <div class=" col-md-4 col-sm-12 well pull-right-lg" style="border:0px solid">
    <p class="well" style="padding:10px; margin-bottom:2px;">
      <span class="glyphicon glyphicon-calendar"></span>  Raspored casova 2018/2019
    </p>
    <div class="col-md-12" style="padding:0px;">
      <br>
        <table class="table table-bordered table-style table-responsive">
          
          <tr>
            <th>Ponedeljak</th>
            <th>Utorak</th>
            <th>Sreda</th>
            <th>Cetvrtak</th>
            <th>Petak</th>
            <th>Subota</th>
            <th>Nedelja</th>
          </tr>
          <tr>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
          </tr>
          <tr>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            <td>11</td>
            <td class="today">12</td>
            <td>13</td>
            <td>14</td>
          </tr>
          <tr>
            <td>15</td>
            <td>16</td>
            <td>17</td>
            <td>18</td>
            <td>19</td>
            <td>20</td>
            <td>21</td>
          </tr>
           <tr>
            <td>22</td>
            <td>23</td>
            <td>24</td>
            <td>25</td>
            <td>26</td>
            <td>27</td>
            <td>28</td>
          </tr>
            <tr>
            <td>29</td>
            <td>30</td>
            <td>31</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          
          <!--?php
            foreach ($weeks as $week) {
              echo $week;
            };
          ?-->
        </table>

    </div>
  </div>
  
<script src="/webjars/jquery/3.2.0/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.3.0/js/bootstrap.min.js"></script>

</body>
</html>