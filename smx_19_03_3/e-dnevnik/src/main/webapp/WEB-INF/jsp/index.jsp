<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title>Welcome</title>
<link href="/webjars/bootstrap/4.3.0/css/bootstrap.min.css"
	rel="stylesheet">

</head>
<body>
	<div class="container">
		<form>
			<div class="form-group">
				<label for="exampleInputEmail1">Korisnik</label> <input
					type="email" class="form-control" id="exampleInputEmail1"
					aria-describedby="emailHelp" placeholder="Korisnik"> <small
					id="emailHelp" class="form-text text-muted"></small>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Sifra</label> <input
					type="password" class="form-control" id="exampleInputPassword1"
					placeholder="Sifra">
			</div>
			<div class="form-check">
				<input type="checkbox" class="form-check-input" id="exampleCheck1">
				<label class="form-check-label" for="exampleCheck1">Izloguj me</label>
			</div>
                    
			<button type="submit" class="btn btn-primary">Potvrdi</button>
		</form>


	</div>
	<script src="/webjars/jquery/3.2.0/jquery.min.js"></script>
	<script src="/webjars/bootstrap/4.3.0/js/bootstrap.min.js"></script>
        
</body>
</html>
